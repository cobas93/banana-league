<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link rel="stylesheet" href="css/banana.css">
    <title>Banana League | Noticias</title>
</head>
<body>
   <!-- Header menu -->
   <?php require 'menu.php'; ?>
   <input type="hidden" id="pagina" value="noticias">

   <!-- Titulo pagina -->
   <div class="tituloPagina">
        <img src="img/grieta_bg.jpg" alt="">
        <h1 class="cabecera">Noticias</h1>
   </div>

   <!-- Contenido -->
   <div id="container">
        <div class="noticia">
            <h2>Titulo de la noticia 1</h2>
            <p>Lorem fistrum está la cosa muy malar diodeno te va a hasé pupitaa se calle ustée ese que llega por la gloria de mi madre ese pedazo de no te digo trigo por no llamarte Rodrigor. Al ataquerl fistro ese que llega por la gloria de mi madre jarl apetecan llevame al sircoo. Qué dise usteer sexuarl a wan tiene musho peligro ahorarr benemeritaar. A gramenawer sexuarl benemeritaar caballo blanco caballo negroorl por la gloria de mi madre ahorarr caballo blanco caballo negroorl a wan de la pradera quietooor ese hombree. Llevame al sircoo papaar papaar pupita ese que llega. Al ataquerl pecador te voy a borrar el cerito la caidita.....</p>
            <a href="#">Ver mas &#x25ba;</a>
        </div>

        <hr>

        <div class="noticia">
            <h2>Titulo de la noticia 2</h2>
            <p>Lorem fistrum está la cosa muy malar diodeno te va a hasé pupitaa se calle ustée ese que llega por la gloria de mi madre ese pedazo de no te digo trigo por no llamarte Rodrigor. Al ataquerl fistro ese que llega por la gloria de mi madre jarl apetecan llevame al sircoo. Qué dise usteer sexuarl a wan tiene musho peligro ahorarr benemeritaar. A gramenawer sexuarl benemeritaar caballo blanco caballo negroorl por la gloria de mi madre ahorarr caballo blanco caballo negroorl a wan de la pradera quietooor ese hombree. Llevame al sircoo papaar papaar pupita ese que llega. Al ataquerl pecador te voy a borrar el cerito la caidita.....</p>
            <a href="#">Ver mas &#x25ba;</a>
        </div>
   </div>

   <!-- Scripts -->
   <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.min.js'></script>
   <script src="js/funciones.js"></script>
   <!-- Footer -->
</body>
</html>