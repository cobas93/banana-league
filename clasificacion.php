<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link rel="stylesheet" href="css/banana.css">
    <title>Banana League | Clasificación</title>
</head>
<body>
   <!-- Header menu -->
   <?php require 'menu.php'; ?>
   <input type="hidden" id="pagina" value="clasificacion">

   <!-- Titulo pagina -->
   <div class="tituloPagina">
        <img src="img/islasdelasombra_bg.jpg" alt="">
        <h1 class="cabecera">Clasificación</h1>
   </div>
   <!-- Contenido -->
   <div id="container">
        <h2 class="liga">Liga 1</h2>
        <table class="clasificacion liga1">
            <thead>
                <tr>
                    <td></td>
                    <td>Equipo</td>
                    <td>Partidos jugados</td>
                    <td>Victorias</td>
                    <td>Derrotas</td>
                    <td>Puntos</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1º</td>
                    <td class="nombre">Attitude Gaming</td>
                    <td class="jugados">3</td>
                    <td class="victorias">2</td>
                    <td class="derrotas">1</td>
                    <td class="puntos"><span class="pts">9</span> pts.</td>
                </tr>
                <tr>
                    <td>2º</td>
                    <td class="nombre">Equipo 2</td>
                    <td class="jugados">3</td>
                    <td class="victorias">1</td>
                    <td class="derrotas">2</td>
                    <td class="puntos"><span class="pts">7</span> pts.</td>
                </tr>
                <tr>
                    <td>3º</td>
                    <td class="nombre">Equipo 4</td>
                    <td class="jugados">2</td>
                    <td class="victorias">1</td>
                    <td class="derrotas">1</td>
                    <td class="puntos"><span class="pts">6</span> pts.</td>
                </tr>
                <tr>
                    <td>4º</td>
                    <td class="nombre">Test Gaming</td>
                    <td class="jugados">2</td>
                    <td class="victorias">0</td>
                    <td class="derrotas">1</td>
                    <td class="puntos"><span class="pts">4</span> pts.</td>
                </tr>
                <tr>
                    <td>5º</td>
                    <td class="nombre">Otro equipo</td>
                    <td class="jugados">1</td>
                    <td class="victorias">0</td>
                    <td class="derrotas">1</td>
                    <td class="puntos"><span class="pts">2</span> pts.</td>
                </tr>
            </tbody>
        </table>
   </div>

    <!-- Scripts -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.min.js'></script>
    <script src="js/funciones.js"></script>

   <!-- Footer -->
</body>
</html>