<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link rel="stylesheet" href="css/banana.css">
    <title>Banana League | Equipos</title>
</head>
<body>
   <!-- Header menu -->
   <?php require 'menu.php'; ?>
   <input type="hidden" id="pagina" value="equipos">

   <!-- Titulo pagina -->
   <div class="tituloPagina">
        <img src="img/shurima_bg.jpg" alt="">
        <h1 class="cabecera">Equipos</h1>
   </div>

   <!-- Contenido -->
   <div id="container" class="equipos">
        <div class="equipo">
            <div class="main">
                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                <h2 class="nombre">Attitude Gaming</h2>
            </div>
            <div class="datos">
                <p>ELO: <span class="elo">Platino</span></p>
                <p>Top: <span class="top">Persona 1 (Nombre en el lol)</span></p>
                <p>Jungla: <span class="jungla">Persona 2 (Nombre en el lol)</span></p>
                <p>Mid: <span class="mid">Persona 3 (Nombre en el lol)</span></p>
                <p>Adc: <span class="adc">Persona 4 (Nombre en el lol)</span></p>
                <p>Apoyo: <span class="apoyo">Persona 5 (Nombre en el lol)</span></p>
                <br>
                <p>Suplente 1: <span class="suplente1">Persona 6 (Nombre en el lol)</span></p>
                <p>Suplente 2: <span class="suplente2">Persona 7 (Nombre en el lol)</span></p>
                <br>
                <p>Coach: <span class="coach">Persona 8 (Nombre en el lol)</span></p>
            </div>
        </div>

        <div class="equipo">
            <div class="main">
                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                <h2 class="nombre">Attitude Gaming</h2>
            </div>
            <div class="datos">
                <p>ELO: <span class="elo">Platino</span></p>
                <p>Top: <span class="top">Persona 1 (Nombre en el lol)</span></p>
                <p>Jungla: <span class="jungla">Persona 2 (Nombre en el lol)</span></p>
                <p>Mid: <span class="mid">Persona 3 (Nombre en el lol)</span></p>
                <p>Adc: <span class="adc">Persona 4 (Nombre en el lol)</span></p>
                <p>Apoyo: <span class="apoyo">Persona 5 (Nombre en el lol)</span></p>
                <br>
                <p>Suplente 1: <span class="suplente1">Persona 6 (Nombre en el lol)</span></p>
                <p>Suplente 2: <span class="suplente2">Persona 7 (Nombre en el lol)</span></p>
                <br>
                <p>Coach: <span class="coach">Persona 8 (Nombre en el lol)</span></p>
            </div>
        </div>
   </div>

   <!-- Scripts -->
   <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.min.js'></script>
   <script src="js/funciones.js"></script>
   <script>
       $(document).ready(function(){
            $('.equipo .main').on('click', function(){
                $(this).parent().find('.datos').toggle('slow');
                $(this).parent().toggleClass('selected');
            });
       });
   </script>
   <!-- Footer -->
</body>
</html>