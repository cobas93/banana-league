// Funciones y helpers para la web banana league
function animateHamburguer(x) {
  x.classList.toggle("change");
}

$(document).ready(function(){
    var pagina = $('#pagina').val();
    $('#menu li.' + pagina +' a').addClass('active');
});

$(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    if(scroll > 500){
        $('header#menu').addClass('scrolling');
    }

    if(scroll < 500){
        $('header#menu').removeClass('scrolling');
    }
});