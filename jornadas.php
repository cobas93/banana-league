<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link rel="stylesheet" href="css/banana.css">
    <title>Banana League | Noticias</title>
</head>
<body>
   <!-- Header menu -->
   <!-- Header menu -->
   <?php require 'menu.php'; ?>
   <input type="hidden" id="pagina" value="clasificacion">

   <!-- Titulo pagina -->
   <div class="tituloPagina">
        <img src="img/grieta_bg.jpg" alt="">
        <h1 class="cabecera">Jornadas</h1>
   </div>
   <!-- Contenido -->
   <div id="container">
        <div class="jornada">
            <h2 class="semana">Semana 1 - <i class="fa fa-calendar" aria-hidden="true"></i> <span class="fechaInicio">04/01/2019</span> - <span class="fechaFin">11/01/2019</span></h2>
            <div class="partido">
                <p class="equipo equipo1">Attitude Gaming</p>
                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">

                <span>VS.</span>

                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                <p class="equipo equipo2">Attitude Gaming</p> 
            </div>

            <div class="partido">
                <p class="equipo equipo1">Attitude Gaming</p>
                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">

                <span>VS.</span>

                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                <p class="equipo equipo2">Attitude Gaming</p> 
            </div>

            <div class="partido">
                <p class="equipo equipo1">Attitude Gaming</p>
                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">

                <span>VS.</span>

                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                <p class="equipo equipo2">Attitude Gaming</p> 
            </div>

            <div class="partido">
                <p class="equipo equipo1">Attitude Gaming</p>
                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">

                <span>VS.</span>

                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                <p class="equipo equipo2">Attitude Gaming</p> 
            </div>
        </div>

        <div class="jornada">
            <h2 class="semana">Semana 2 - <i class="fa fa-calendar" aria-hidden="true"></i> <span class="fechaInicio">12/01/2019</span> - <span class="fechaFin">19/01/2019</span></h2>
            <div class="partido">
                <p class="equipo equipo1">Attitude Gaming</p>
                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">

                <span>VS.</span>

                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                <p class="equipo equipo2">Attitude Gaming</p> 
            </div>

            <div class="partido">
                <p class="equipo equipo1">Attitude Gaming</p>
                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">

                <span>VS.</span>

                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                <p class="equipo equipo2">Attitude Gaming</p> 
            </div>

            <div class="partido">
                <p class="equipo equipo1">Attitude Gaming</p>
                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">

                <span>VS.</span>

                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                <p class="equipo equipo2">Attitude Gaming</p> 
            </div>

            <div class="partido">
                <p class="equipo equipo1">Attitude Gaming</p>
                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">

                <span>VS.</span>

                <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                <p class="equipo equipo2">Attitude Gaming</p> 
            </div>
        </div>
   </div>
   <!-- Footer -->
</body>
</html>