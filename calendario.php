<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link rel="stylesheet" href="css/banana.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <title>Banana League | Calendario</title>
</head>
<body>
   <!-- Header menu -->
   <?php require 'menu.php'; ?>
   <input type="hidden" id="pagina" value="calendario">

   <!-- Titulo pagina -->
   <div class="tituloPagina">
        <img src="img/abismo_bg.jpg" alt="">
        <h1 class="cabecera">Calendario</h1>
   </div>
   <!-- Contenido -->
   <div id="container">
        <div class="seleccion">
            <h2 class="verProximosPartidos selected">Próximos partidos</h2>
            <h2 class="verPartidosPasados">Partidos pasados</h2>
        </div>
        <div class="proximosPartidos">
            <div class="partido">
                <p class="fecha"><i class="fa fa-calendar" aria-hidden="true"></i> <span class="fechaPartido">29/12/2018</span> - <i class="far fa-clock"></i> <span class="horaPartido">16:00</span></p>
                <div style="text-align: center">
                    <h2 class="equipo equipo1">Attitude Gaming</h2>
                    <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">

                    <span>VS.</span>

                    <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                    <h2 class="equipo equipo2">Attitude Gaming</h2>                    
                </div>
                <a href="#" class="twitch"><i class="fab fa-twitch"></i> Ver partido</a>
            </div>
            <div class="partido">
                <p class="fecha"><i class="fa fa-calendar" aria-hidden="true"></i> <span class="fechaPartido">04/01/2019</span> - <i class="far fa-clock"></i> <span class="horaPartido">19:15</span></p>
                <div style="text-align: center">
                    <h2 class="equipo equipo1">Attitude Gaming</h2>
                    <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">

                    <span>VS.</span>

                    <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                    <h2 class="equipo equipo2">Attitude Gaming</h2>                    
                </div>
                <a href="#" class="twitch"><i class="fab fa-twitch"></i> Ver partido</a>
            </div>
        </div>
        
        <div class="partidosPasados" style="display:none;">
            <div class="partido">
                <p class="fecha"><i class="fa fa-calendar" aria-hidden="true"></i> <span class="fechaPartido">26/12/2018</span> - <i class="far fa-clock"></i> <span class="horaPartido">12:30</span></p>
                <div style="text-align: center">
                    <h2 class="equipo equipo1 ganador">Attitude Gaming</h2>
                    <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">

                    <span>VS.</span>

                    <img src="https://images-ext-2.discordapp.net/external/oEE5JtDXC1M5c_0nGMj169jCimWuwiVA8VMXW3E9shM/https/yt3.ggpht.com/a-/AAuE7mAGYRbWEKwhTW920XRUcB9kdHoy7MYmnVr2%3Ds900-mo-c-c0xffffffff-rj-k-no?width=553&height=553" class="logoEquipo" alt="">
                    <h2 class="equipo equipo2 perdedor">Attitude Gaming</h2>                    
                </div>
                <a href="https://matchhistory.euw.leagueoflegends.com/es/#match-details/EUW1/3874893176/31013470" target="_blank" class="overview"><i class="fas fa-history"></i> Ver historial</a>
            </div>
        </div>
   </div>

    <!-- Scripts -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.min.js'></script>
    <script src="js/funciones.js"></script>
    <script>
        $(document).ready(function(){
            $('.verPartidosPasados').on('click', function(){
                $(this).addClass('selected');
                $('.verProximosPartidos').removeClass('selected');
                $('.partidosPasados').show();
                $('.proximosPartidos').hide();
            });

            $('.verProximosPartidos').on('click', function(){
                $(this).addClass('selected');
                $('.verPartidosPasados').removeClass('selected');
                $('.proximosPartidos').show();
                $('.partidosPasados').hide();
            });
        });
    </script>
   <!-- Footer -->
</body>
</html>